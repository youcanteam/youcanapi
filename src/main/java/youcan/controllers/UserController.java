package youcan.controllers;

import youcan.controllers.models.*;
import youcan.entities.*;
import youcan.services.NotificationService;
import youcan.services.UserService;
import youcan.utils.*;

import static spark.Spark.*;

import java.util.Date;

import com.google.gson.Gson;


public class UserController extends BaseController {

	public UserController(final UserService userService, final NotificationService notificationService) {

		path("/api", () -> {
			get("/users", (request, res) -> userService.getAllUsers(), new JsonTransformer());

	        // Gets the user resource for the provided id
	        get("/users/:id", (request, response) -> {
	            UserModel user = userService.getUserModel(Integer.parseInt(request.params(":id")));
	            if (user != null) {
	            		return user;
	            } else {
	                response.status(404); // 404 Not found
	                return "User not found";
	            }
	        }, new JsonTransformer());

//			post("/users", (request, response) -> {
			post("/register", (request, response) -> {
				
				User user = new Gson().fromJson(request.body(), User.class);
	            user.setPictureUrl("https://yyoucan.blob.core.windows.net/gesphotos/profils.gif");
	            user = userService.createUser(user);
	            
	            return user;
	        }, new JsonTransformer());
			
			post("/login", (request, resonse) -> {
				UserLogin userLogin = new Gson().fromJson(request.body(), UserLogin.class);
	            User user = userService.isUserExist(userLogin);
	            if(user != null)
	            		return user;
	            else return new StandardResponse(StatusResponse.ERROR, "Login ou mot de passe est incorrect");
			}, new JsonTransformer());
			
	        put("/users/:id", (request, response) -> {

				User user = new Gson().fromJson(request.body(), User.class);
	            user = userService.updateUser(user);
	            
	            return user;
	        }, new JsonTransformer());
			
	        post("/users/:id/contacts/:contactToAddId", (request, response) -> {

	            User user = userService.getUser(Integer.parseInt(request.params(":id")));
	            int contactToAddId = Integer.parseInt(request.params(":contactToAddId"));
	            user = userService.updateUserAddContact(user, contactToAddId);

				Notification newNotif = new Notification();
				newNotif.setMessage(String.format("<b>%s</b> vous a envoyé une invitation de connexion.", user.getFullName()));
				newNotif.setNotificationDatetime(new Date());
//				newNotif.setEntityId(user.getIdUser());
				newNotif.setEntityName("User");
				newNotif.setUserId(contactToAddId);
				newNotif.setUser(new User(contactToAddId));
				newNotif.setSenderId(user.getIdUser());
				newNotif.setSender(new User(user.getIdUser()));
				newNotif.setCanShowInBadge(true);
				newNotif.setRequestAccepted(false);
				notificationService.createNotification(newNotif);
				
	            return user;
	        }, new JsonTransformer());
		});
	    
	  }

}
