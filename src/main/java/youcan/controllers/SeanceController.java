package youcan.controllers;

import youcan.entities.Course;
import youcan.entities.Seance;
import youcan.entities.User;
import youcan.services.CourseService;
import youcan.services.SeanceService;
import youcan.utils.JsonTransformer;
import static spark.Spark.*;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class SeanceController extends BaseController {

	public SeanceController(final SeanceService seanceService, final CourseService courseService) {
		path("/api", () -> {

			// CRUD Seances
			// Get seances by courseId
			get("/courses/:id/seances", (request, response) -> {
				Course course = courseService.getCourse(Integer.parseInt(request.params(":id")));
				if (course != null) {
					return course.getSeances();
				} else {
					response.status(404); // 404 Not found
					return "Seance not found";
				}
			}, new JsonTransformer());
			
			// Get seance by seanceId
			get("/seances/:id", (request, response) -> {
				int id = Integer.parseInt(request.params(":id"));
				Seance seance = seanceService.getSeance(id);
				if (seance != null) {
					return seance;
				} else {
					response.status(404);
					return "Seance not found";
				}
			}, new JsonTransformer());

			post("/courses/:id/seances", (request, response) -> {

				Integer courseId = Integer.parseInt(request.params(":id"));
				Gson gSon = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
				Seance seance = gSon.fromJson(request.body(), Seance.class);
				seance.setCourse(new Course(courseId));
				seance = seanceService.createSeance(seance);

				return seance;
			}, new JsonTransformer());

			put("/seances/:id", (request, response) -> {
				Gson gSon = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
				Seance seance = gSon.fromJson(request.body(), Seance.class);
				seance.setId(Integer.parseInt(request.params(":id")));
				seance = seanceService.updateSeance(seance);

				return seance;
			}, new JsonTransformer());

			delete("/seances/:id", (request, response) -> {
				int id = Integer.parseInt(request.params(":id"));
				Seance seance = seanceService.getSeance(id);
				if (seance != null) {
					seanceService.deleteSeance(id);
					return "Seance with id " + id + " is deleted!";
				} else {
					response.status(404);
					return "Seance not found";
				}
			}, new JsonTransformer());

		});
	}
}
