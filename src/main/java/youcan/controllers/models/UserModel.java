package youcan.controllers.models;
import java.io.Serializable;
import java.util.*;

import javax.persistence.*;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.google.gson.annotations.Expose;

import youcan.entities.*;

public class UserModel implements Serializable {

	private int idUser;
	private String firstName;
	private String lastName;
	private String pictureUrl;
	private String email;
	private String speciality;
	private String sexe;
	private Date birthdate;
	private String country;
	private String city;
	private String about;
	private String password;

	private List<Course> courses = new ArrayList<>();
	
	private List<Comment> comments = new ArrayList<>();

	private List<User> contacts = new ArrayList<User>();
	
	private List<User> users = new ArrayList<User>();

	private List<Notification> notifications = new ArrayList<>();

	private List<Notification> sentNotifications = new ArrayList<>();

	public UserModel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserModel(int id) {
		super();
		this.idUser = id;
	}

	public UserModel(String firstName, String lastName, String email, String speciality, String sexe, Date birthdate,
			String country, String city, String about, String password) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.speciality = speciality;
		this.sexe = sexe;
		this.birthdate = birthdate;
		this.country = country;
		this.city = city;
		this.about = about;
		this.password = password;
	}

	public UserModel(User user) {

		this.idUser = user.getIdUser();
		this.firstName = user.getFirstName();
		this.lastName = user.getLastName();
		this.email = user.getEmail();
		this.pictureUrl = user.getPictureUrl();
		this.speciality = user.getSpeciality();
		this.sexe = user.getSexe();
		this.birthdate = user.getBirthdate();
		this.country = user.getCountry();
		this.city = user.getCity();
		this.about = user.getAbout();
		this.password = user.getPassword();

		this.contacts = user.getContacts();
		this.users = user.getUsers();
		this.notifications = user.getNotifications();
		this.sentNotifications = user.getSentNotifications();
	}

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFullName() {
		return  this.firstName + " " + this.lastName;
	}
	
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPictureUrl() {
		return pictureUrl;
	}

	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSpeciality() {
		return speciality;
	}

	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}

	public String getSexe() {
		return sexe;
	}

	public void setSexe(String sexe) {
		this.sexe = sexe;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public void addContact(User contact) {
		this.contacts.add(contact);
	}

}
