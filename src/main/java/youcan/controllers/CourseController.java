package youcan.controllers;

import youcan.entities.Course;
import youcan.entities.Seance;
import youcan.entities.User;
import youcan.services.CourseService;
import youcan.utils.JsonTransformer;
import static spark.Spark.*;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class CourseController extends BaseController {

	public CourseController(final CourseService courseService) {
		path("/api", () -> {

		    // CRUD Courses
		    get("/courses", (req, res) -> courseService.getAllCourses(), new JsonTransformer());

	        get("/courses/:id", (request, response) -> {
	            Course course = courseService.getCourse(Integer.parseInt(request.params(":id")));
	            if (course != null) {
	            		return course;
	            } else {
	                response.status(404); // 404 Not found
	                return "Course not found";
	            }
	        }, new JsonTransformer());
	        
			post("/courses", (request, response) -> {
				Gson gSon =  new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
				Course course = gSon.fromJson(request.body(), Course.class);

				course.setInstructor(new User(course.getIntructorId()));
				course = courseService.createCourse(course);
				// Setup course seances
				for(int i = 0; i< course.getSeanceNumber(); i++) {
					course.getSeances().add(new Seance(course));
				}
				course = courseService.updateCourse(course);
	            
	            return course;
	        }, new JsonTransformer());
	        
			put("/courses/:id", (request, response) -> {
				Gson gSon =  new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
				Course course = gSon.fromJson(request.body(), Course.class);
				course = courseService.updateCourse(course);
	            
	            return course;
	        }, new JsonTransformer());

			delete("/courses/:id", (request, response) -> {
				int id = Integer.parseInt(request.params(":id"));
				Course course = courseService.getCourse(id);
				if (course != null) {
					courseService.deleteCourse(id);
					return "Course with id " + id + " is deleted!";
				} else {
					response.status(404);
					return "Course not found";
				}
			}, new JsonTransformer());
			
		});
	  }
}
