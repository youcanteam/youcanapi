package youcan.controllers;

import youcan.entities.*;
import youcan.services.CourseService;
import youcan.services.NotificationService;
import youcan.utils.JsonTransformer;
import static spark.Spark.*;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class NotificationController extends BaseController {

	public NotificationController(final NotificationService notificationService) {
		path("/api", () -> {

		    // CRUD Notifications
		    get("/users/:id/notifications", (request, response) -> {
		    		List<Notification> notifications = notificationService.getAllNotificationsByUserId(Integer.parseInt(request.params(":id")));
				if (notifications != null) {
					return notifications;
				} else {
					response.status(404); // 404 Not found
					return "Notifications not found";
				}
			}, new JsonTransformer());
		    
		    post("/notifications", (request, response) -> {
				Gson gSon =  new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
				Type listType = new TypeToken<List<Notification>>(){}.getType();
				List<Notification> notifications = (List<Notification>) gSon.fromJson(request.body(), listType);
			
				for (Notification notif : notifications) {
					notif.setCanShowInBadge(true);
					notif.setSender(new User(notif.getSenderId()));
					notif.setUser(new User(notif.getUserId()));
					notif.setNotificationDatetime(new Date());
					notificationService.createNotification(notif);
				}
	            
	            return notifications;
	        }, new JsonTransformer());
		    
		    get("/users/:id/notifications/count", (request, response) -> {
		    		int usersNotificationsCount = notificationService.getNotificationsCountByUserId(Integer.parseInt(request.params(":id")));
		    		return usersNotificationsCount;
			}, new JsonTransformer());

	        get("/notifications/:id", (request, response) -> {
	            Notification notification = notificationService.getNotification(Integer.parseInt(request.params(":id")));
	            if (notification != null) {
	            		return notification;
	            } else {
	                response.status(404); // 404 Not found
	                return "Course not found";
	            }
	        }, new JsonTransformer());

	        put("/notifications/:id", (request, response) -> {
	            Notification notification = notificationService.getNotification(Integer.parseInt(request.params(":id")));
	            if (notification != null) {
	            		notificationService.updateNotification(notification);
	            		return notification;
	            } else {
	                response.status(404); // 404 Not found
	                return "Course not found";
	            }
	        }, new JsonTransformer());
		});
	  }
}
