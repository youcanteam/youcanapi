package youcan.controllers;

import youcan.entities.Course;
import youcan.entities.Notification;
import youcan.entities.Comment;
import youcan.entities.User;
import youcan.services.CourseService;
import youcan.services.NotificationService;
import youcan.services.CommentService;
import youcan.utils.JsonTransformer;
import static spark.Spark.*;

import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class CommentController extends BaseController {

	public CommentController(final CommentService commentService, final CourseService courseService,
			final NotificationService notificationService  
			) {
		path("/api", () -> {

			// CRUD Comments
			// Get comments by courseId
			get("/courses/:id/comments", (request, response) -> {
				Course course = courseService.getCourse(Integer.parseInt(request.params(":id")));
				if (course != null) {
					return course.getComments();
				} else {
					response.status(404); // 404 Not found
					return "Course not found";
				}
			}, new JsonTransformer());
			
			// Get comment by commentId
			get("/comments/:id", (request, response) -> {
				int id = Integer.parseInt(request.params(":id"));
				Comment comment = commentService.getComment(id);
				if (comment != null) {
					return comment;
				} else {
					response.status(404);
					return "comment not found";
				}
			}, new JsonTransformer());

			post("/courses/:id/comments/:senderId", (request, response) -> {

				Integer courseId = Integer.parseInt(request.params(":id"));
				Integer senderId = Integer.parseInt(request.params(":senderId"));
				Gson gSon = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
				Comment comment = gSon.fromJson(request.body(), Comment.class);
				comment.setCourse(new Course(courseId));
				comment.setInstructor(new User(comment.getInstructorId()));
				comment.setDatePublished(new Date());
				comment = commentService.createComment(comment);

				Course course = courseService.getCourse(Integer.parseInt(request.params(":id")));
				if(course.getInstructor().getIdUser() != senderId) {
					// new notification response
					Notification newNotif = new Notification();
					newNotif.setNotificationDatetime(new Date());
					newNotif.setEntityId(course.getId());
					newNotif.setEntityName("Info");// oldNotification.getEntityName());
					newNotif.setUserId(course.getInstructor().getIdUser());
					newNotif.setUser(new User(course.getInstructor().getIdUser()));
					newNotif.setCanShowInBadge(true);
					newNotif.setRequestAccepted(true);
					newNotif.setMessage(String.format("<b>%s</b> a commenté votre cours.", comment.getInstructorName()));
					notificationService.createNotification(newNotif);
				}
				
				return comment;
			}, new JsonTransformer());


			delete("/comments/:id", (request, response) -> {
				int id = Integer.parseInt(request.params(":id"));
				Comment comment = commentService.getComment(id);
				if (comment != null) {
					commentService.deleteComment(id);
					return "comment with id " + id + " is deleted!";
				} else {
					response.status(404);
					return "comment not found";
				}
			}, new JsonTransformer());

		});
	}
}
