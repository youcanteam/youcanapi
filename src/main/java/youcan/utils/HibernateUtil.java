package youcan.utils;

import org.hibernate.*;
import org.hibernate.cfg.*;

import youcan.entities.Course;
import youcan.entities.User;

public class HibernateUtil {

//    private static final SessionFactory sessionFactory;
//
//    static {
//        try {
//            // Create the SessionFactory from hibernate.cfg.xml
//            sessionFactory = new Configuration()
//            			.configure()
//					.addAnnotatedClass(Course.class)
//					.buildSessionFactory();
//        } catch (Throwable ex) {
//            // Make sure you log the exception, as it might be swallowed
//            System.err.println("Initial SessionFactory creation failed." + ex);
//            throw new ExceptionInInitializerError(ex);
//        }
//    }
//
//    public static SessionFactory getSessionFactory() {
//        return sessionFactory;
//    }

    public static Session getHibernateSession() {
    
        final SessionFactory sf = new Configuration()
            .configure("hibernate.cfg.xml").buildSessionFactory();
    
        // factory = new Configuration().configure().buildSessionFactory();
        final Session session = sf.openSession();
        return session;
    }

}