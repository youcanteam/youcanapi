package youcan.utils;

import static spark.Spark.after;
import static spark.Spark.options;

/**
 * Simple helper for enabling CORS in a spark application;
 */
public final class CorsFilter {

	public static void enableCORS(final String origin, final String methods, final String headers) {

	    options("/*", (request, response) -> {

	        String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
	        if (accessControlRequestHeaders != null) {
	            response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
	        }

	        String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
	        if (accessControlRequestMethod != null) {
	            response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
	        }

	        return "OK";
	    });

	    after((request, response) -> {
	        response.header("Access-Control-Allow-Origin", "*");
	        response.header("Access-Control-Request-Method", "GET,PUT,POST,DELETE,OPTIONS");
	        response.header("Access-Control-Allow-Headers", "Content-Type");
	        response.header("Access-Control-Allow-Credentials", "true");
	        response.header("Content-Type", "application/json");
	    });
	}
    
}