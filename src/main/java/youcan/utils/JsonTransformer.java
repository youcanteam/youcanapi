package youcan.utils;

import java.lang.reflect.Modifier;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import spark.ResponseTransformer;

public class JsonTransformer implements ResponseTransformer {
	
//	Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
	Gson gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.PUBLIC).setPrettyPrinting().create();
//	Gson gson = new GsonBuilder().create();

	public String render(Object model) {
		return gson.toJson(model);
	}
}