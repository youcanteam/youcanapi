package youcan.services;


import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import youcan.entities.*;
import youcan.utils.HibernateUtil;

public class CommentService {

	private static SessionFactory sessionFactory;

	public CommentService() {
		try {
			// Create the SessionFactory from hibernate.cfg.xml
			sessionFactory = new Configuration().configure().addAnnotatedClass(Course.class).buildSessionFactory();
		} catch (Throwable ex) {
			// Make sure you log the exception, as it might be swallowed
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public Comment getComment(int id) {
		// create session
		Session session = HibernateUtil.getHibernateSession();
		Comment comment = null;
		try {

			// start a transaction
			session.beginTransaction();

			comment = (Comment) session.get(Comment.class, id);

			// commit transaction
			session.getTransaction().commit();
		} finally {
			session.close();
		}
		return comment;
	}

	// creates a new comment
	public Comment createComment(Comment comment) {
		// create session
		Session session = HibernateUtil.getHibernateSession();

		try {
			// start a transaction
			session.beginTransaction();

			// save the course object
			session.save(comment);

			// commit transaction
			session.getTransaction().commit();
		} finally {
			session.close();
		}
		return comment;
	}

	// Delete an existing Comment
	public Comment deleteComment(int commentId) {
		// create session
		Session session = HibernateUtil.getHibernateSession();

		Comment oldComment = null;
		try {
			// start a transaction
			session.beginTransaction();

			oldComment = (Comment) session.get(Comment.class, commentId);
			// save the course object
			session.delete(oldComment);

			// commit transaction
			session.getTransaction().commit();
		} finally {
			session.close();
		}
		return oldComment;
	}

}
