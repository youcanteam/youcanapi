package youcan.services;

import java.util.*;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import youcan.controllers.models.UserModel;
import youcan.entities.*;
import youcan.utils.HibernateUtil;

public class UserService {

	private static SessionFactory sessionFactory;

	public UserService() {

		try {
			// Create the SessionFactory from hibernate.cfg.xml
			sessionFactory = new Configuration().configure().addAnnotatedClass(User.class).buildSessionFactory();
		} catch (Throwable ex) {
			// Make sure you log the exception, as it might be swallowed
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	// returns a list of all users
	public List<UserModel> getAllUsers() {
		// create session
		Session session = HibernateUtil.getHibernateSession();
		List<User> usersDTO = null;
		List<UserModel> users = null;
		try {

			// start a transaction
			session.beginTransaction();

			usersDTO = session.createQuery("from User").list();
			users = new ArrayList();
			for (User user : usersDTO) {
				users.add(new UserModel(user));
			}

			// commit transaction
			session.getTransaction().commit();
		} finally {
			session.close();
		}
		return users;
	}

	public UserModel getUserModel(int id) {
		// create session
		Session session = HibernateUtil.getHibernateSession();
		UserModel user = null;
		try {

			// start a transaction
			session.beginTransaction();

			User userDTO = (User) session.get(User.class, id);
			if(userDTO == null)
				return null;
			user = new UserModel(userDTO);

			// commit transaction
			session.getTransaction().commit();
		} finally {
			session.close();
		}
		return user;
	}
	
	// returns a single user by id
	public User getUser(int id) {
		// create session
		Session session = HibernateUtil.getHibernateSession();
		User user = null;
		try {

			// start a transaction
			session.beginTransaction();

			user = (User) session.get(User.class, id);

			// commit transaction
			session.getTransaction().commit();
		} finally {
			session.close();
		}
		return user;
	}

	// creates a new user
	public User createUser(User user) {
		// create session

		Session session = HibernateUtil.getHibernateSession();

		try {

			// start a transaction
			session.beginTransaction();

			// save the user object
			Integer id = (Integer) session.save(user);
			user = session.get(User.class, id);

			// commit transaction
			session.getTransaction().commit();
		} finally {
			session.close();
		}
		return user;
	}

	// updates an existing user
	public User updateUser(User user) {
		// create session

		Session session = HibernateUtil.getHibernateSession();

		User oldUser = null;
		try {
			// start a transaction
			session.beginTransaction();

			oldUser = (User) session.get(User.class, user.getIdUser());
			oldUser.setFirstName(user.getFirstName());
			oldUser.setLastName(user.getLastName());
			oldUser.setSexe(user.getSexe());
			oldUser.setSpeciality(user.getSpeciality());
			oldUser.setCity(user.getCity());
			oldUser.setCountry(user.getCountry());
			oldUser.setAbout(user.getAbout());

			// save the user object
			session.save(oldUser);

			// commit transaction
			session.getTransaction().commit();
		} finally {
			session.close();
		}
		return oldUser;
	}

	// updates an existing user
	public User updateUserAddContact(User user, int contactToAddId) {
		// create session
		Session session = HibernateUtil.getHibernateSession();

		User oldUser = null;
		try {
			// start a transaction
			session.beginTransaction();

			oldUser = (User) session.get(User.class, user.getIdUser());
			User contactToAdd = (User) session.get(User.class, contactToAddId);
			oldUser.addContact(contactToAdd);
            
			// save the user object
			session.save(oldUser);

			// commit transaction
			session.getTransaction().commit();
		} finally {
			session.close();
		}
		return oldUser;
	}

	// creates a new user
	public User connect(User user) {
		// create session

		Session session = HibernateUtil.getHibernateSession();

		try {

			// start a transaction
			session.beginTransaction();

			// save the user object
			session.save(user);

			// commit transaction
			session.getTransaction().commit();
		} finally {
			session.close();
		}
		return user;
	}

	// public boolean isUserExist(UserLogin userLogin) {
	public User isUserExist(UserLogin userLogin) {
		// create session

		Session session = HibernateUtil.getHibernateSession();
		User user = null;
		try {

			// start a transaction
			session.beginTransaction();

			Query query = session.createQuery("FROM User u WHERE u.email = :username");
			query.setParameter("username", userLogin.getUsername());

			// user = (User) query.uniqueResult()[0];
			user = (User) query.setMaxResults(1).uniqueResult();
			// commit transaction
			session.getTransaction().commit();

			// Check passwords
			if (user != null && user.getPassword().equals(userLogin.getPassword()))
				return user;
			else
				return null;

		} finally {
			session.close();
		}
	}

}
