package youcan.services;

import java.util.*;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import youcan.entities.*;
import youcan.utils.HibernateUtil;

public class CourseService {

	private static SessionFactory sessionFactory;

	public CourseService() {
		try {
			// Create the SessionFactory from hibernate.cfg.xml
			sessionFactory = new Configuration().configure().addAnnotatedClass(Course.class).buildSessionFactory();
		} catch (Throwable ex) {
			// Make sure you log the exception, as it might be swallowed
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	// returns a list of all courses
	public List<Course> getAllCourses() {
		// create session
		Session session = HibernateUtil.getHibernateSession();
		List<Course> theCourses = null;
		try {

			// start a transaction
			session.beginTransaction();

			theCourses = session.createQuery("FROM Course ORDER BY dateCreation DESC").list();

			// commit transaction
			session.getTransaction().commit();
		} finally {
			session.close();
		}
		return theCourses;
	}

	// returns a single course by id
	public Course getCourse(int id) {
		// create session
		Session session = HibernateUtil.getHibernateSession();
		Course course = null;
		try {

			// start a transaction
			session.beginTransaction();

			course = (Course) session.get(Course.class, id);

			// commit transaction
			session.getTransaction().commit();
		} finally {
			session.close();
		}
		return course;
	}

	// creates a new course
	public Course createCourse(Course course) {
		// create session
		Session session = HibernateUtil.getHibernateSession();

		try {
			// start a transaction
			session.beginTransaction();

			course.setInstructor(new User(course.getIntructorId()));
			// save the course object
			session.save(course);

			// commit transaction
			session.getTransaction().commit();
		} finally {
			session.close();
		}
		return course;
	}

	// updates an existing Course
	public Course updateCourse(Course course) {
		// create session
		Session session = HibernateUtil.getHibernateSession();

		Course oldCourse = null;
		try {
			// start a transaction
			session.beginTransaction();

			oldCourse = (Course) session.get(Course.class, course.getId());
			oldCourse.setTitle(course.getTitle());
			oldCourse.setLangage(course.getLangage());
			oldCourse.setEndDate(course.getEndDate());
			oldCourse.setDomaine(course.getDomaine());
			oldCourse.setDescription(course.getDescription());
			oldCourse.setSeanceNumber(course.getSeanceNumber());
			oldCourse.setPrice(course.getPrice());
			oldCourse.setMinNbrContributors(course.getMinNbrContributors());
			oldCourse.setMaxNbrContributors(course.getMaxNbrContributors());
			oldCourse.setSeances(course.getSeances());

			// save the course object
			session.save(oldCourse);

			// commit transaction
			session.getTransaction().commit();
		} finally {
			session.close();
		}
		return oldCourse;
	}

	// Delete an existing Course
	public Course deleteCourse(int courseId) {
		// create session
		Session session = HibernateUtil.getHibernateSession();

		Course oldCourse = null;
		try {
			// start a transaction
			session.beginTransaction();

			oldCourse = (Course) session.get(Course.class, courseId);
			// save the course object
			session.delete(oldCourse);

			// commit transaction
			session.getTransaction().commit();
		} finally {
			session.close();
		}
		return oldCourse;
	}
}
