package youcan.services;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import youcan.entities.*;
import youcan.utils.HibernateUtil;

public class SeanceService {

	private static SessionFactory sessionFactory;

	public SeanceService() {
		try {
			// Create the SessionFactory from hibernate.cfg.xml
			sessionFactory = new Configuration().configure().addAnnotatedClass(Course.class).buildSessionFactory();
		} catch (Throwable ex) {
			// Make sure you log the exception, as it might be swallowed
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public Seance getSeance(int id) {
		// create session
		Session session = HibernateUtil.getHibernateSession();
		Seance seance = null;
		try {

			// start a transaction
			session.beginTransaction();

			seance = (Seance) session.get(Seance.class, id);

			// commit transaction
			session.getTransaction().commit();
		} finally {
//			HibernateUtil.getSessionFactory().close();
		}
		return seance;
	}

	// creates a new seance
	public Seance createSeance(Seance seance) {
		// create session
		Session session = HibernateUtil.getHibernateSession();

		try {
			// start a transaction
			session.beginTransaction();

			// save the course object
			session.save(seance);

			// commit transaction
			session.getTransaction().commit();
		} finally {
			session.close();
		}
		return seance;
	}

	// updates an existing Seance
	public Seance updateSeance(Seance seance) {
		// create session
		Session session = HibernateUtil.getHibernateSession();

		Seance oldSeance = null;
		try {
			// start a transaction
			session.beginTransaction();

			oldSeance = (Seance) session.get(Seance.class, seance.getId());
			oldSeance.setTitle(seance.getTitle());
			oldSeance.setDescription(seance.getDescription());
			oldSeance.setStartDate(seance.getStartDate());
			oldSeance.setStartHour(seance.getStartHour());

			// save the course object
			session.save(oldSeance);

			// commit transaction
			session.getTransaction().commit();
		} finally {
			session.close();
		}
		return oldSeance;
	}

	// Delete an existing Seance
	public Seance deleteSeance(int seanceId) {
		// create session
		Session session = HibernateUtil.getHibernateSession();

		Seance oldSeance = null;
		try {
			// start a transaction
			session.beginTransaction();

			oldSeance = (Seance) session.get(Seance.class, seanceId);
			// save the course object
			session.delete(oldSeance);

			// commit transaction
			session.getTransaction().commit();
		} finally {
			session.close();
		}
		return oldSeance;
	}

}
