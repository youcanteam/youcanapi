package youcan.services;

import java.util.*;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import youcan.entities.*;
import youcan.utils.HibernateUtil;

public class NotificationService {

	private static SessionFactory sessionFactory;

	public NotificationService() {
		try {
			// Create the SessionFactory from hibernate.cfg.xml
			sessionFactory = new Configuration().configure().addAnnotatedClass(Notification.class)
					.buildSessionFactory();
		} catch (Throwable ex) {
			// Make sure you log the exception, as it might be swallowed
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	// returns a list of all courses
	public List<Notification> getAllNotifications() {
		// create session
		Session session = HibernateUtil.getHibernateSession();
		List<Notification> theNotifications = null;
		try {
			// start a transaction
			session.beginTransaction();

			theNotifications = session.createQuery("from Notification").list();

			// commit transaction
			session.getTransaction().commit();
		} finally {
			session.close();
		}
		return theNotifications;
	}

	// returns a list of all courses
	public List<Notification> getAllNotificationsByUserId(int userId) {
		// create session
		Session session = HibernateUtil.getHibernateSession();
		List<Notification> notifications = null;
		try {
			// start a transaction
			session.beginTransaction();

			notifications = session.createQuery("from Notification where user_id = :userId ORDER BY notificationDatetime DESC")
					.setParameter("userId", userId).list();

			// commit transaction
			session.getTransaction().commit();
		} finally {
			session.close();
		}
		return notifications;
	}

	// returns a single course by id
	public Notification getNotification(int id) {
		// create session
		Session session = HibernateUtil.getHibernateSession();
		Notification course = null;
		try {

			// start a transaction
			session.beginTransaction();

			course = (Notification) session.get(Notification.class, id);

			// commit transaction
			session.getTransaction().commit();
		} finally {
			session.close();
		}
		return course;
	}

	// returns a single course by id
	public int getNotificationsCountByUserId(int userId) {
		// create session
		Session session = HibernateUtil.getHibernateSession();
		int notificationsCount = 0;
		try {

			// start a transaction
			session.beginTransaction();

			List<Notification> notifications = null;
			notifications = session.createQuery("from Notification where user_id = :userId")
					.setParameter("userId", userId).list();
			notificationsCount = notifications.size();

			// commit transaction
			session.getTransaction().commit();
		} finally {
			session.close();
		}
		return notificationsCount;
	}

	// creates a new course
	public Notification createNotification(Notification notif) {
		// create session
		Session session = HibernateUtil.getHibernateSession();

		try {
			// start a transaction
			session.beginTransaction();

			// save the notif object
			session.save(notif);

			// commit transaction
			session.getTransaction().commit();
		} finally {
			session.close();
		}
		return notif;
	}

	public Notification updateNotification(Notification notif) {
		// create session
		Session session = HibernateUtil.getHibernateSession();

		Notification oldNotification = null;
		try {
			// start a transaction
			session.beginTransaction();

			oldNotification = (Notification) session.get(Notification.class, notif.getId());
			oldNotification.setCanShowInBadge(false);
			oldNotification.setRequestAccepted(true);

			// save the course object
			session.save(oldNotification);
			// commit transaction
			session.getTransaction().commit();

			// new notification response
			Notification newNotif = new Notification();
			newNotif.setNotificationDatetime(new Date());
			newNotif.setEntityId(oldNotification.getEntityId());
			newNotif.setEntityName("Info");// oldNotification.getEntityName());
			newNotif.setUserId(oldNotification.getSenderId());
			newNotif.setUser(new User(oldNotification.getSenderId()));
			newNotif.setCanShowInBadge(true);
			newNotif.setRequestAccepted(true);
			if(oldNotification.getEntityName().equals("Course")) {
				newNotif.setMessage(String.format("<b>%s</b> a accepter votre invitation pour rejoindre votre cours.",
						oldNotification.getUser().getFullName()));
			} else if(oldNotification.getEntityName().equals("User")) {
				newNotif.setMessage(String.format("<b>%s</b> a accepter votre invitation de connexion.",
						oldNotification.getUser().getFullName()));
			}

			// save new notification
			this.createNotification(newNotif);

		} finally {
			session.close();
		}
		return oldNotification;
	}
	//
	// // updates an existing Notification
	//
	// public Notification updateNotification(Notification course) {
	// // create session
	// Session session = HibernateUtil.getHibernateSession();
	//
	// Notification oldNotification = null;
	// try {
	// // start a transaction
	// session.beginTransaction();
	//
	// oldNotification = (Notification) session.get(Notification.class,
	// course.getId());
	// oldNotification.setTitle(course.getTitle());
	// oldNotification.setLangage(course.getLangage());
	// oldNotification.setEndDate(course.getEndDate());
	// oldNotification.setDomaine(course.getDomaine());
	// oldNotification.setDescription(course.getDescription());
	// oldNotification.setSeanceNumber(course.getSeanceNumber());
	// oldNotification.setPrice(course.getPrice());
	// oldNotification.setMinNbrContributors(course.getMinNbrContributors());
	// oldNotification.setMaxNbrContributors(course.getMaxNbrContributors());
	// oldNotification.setSeances(course.getSeances());
	//
	// // save the course object
	// session.save(oldNotification);
	//
	// // commit transaction
	// session.getTransaction().commit();
	// } finally {
	// session.close();
	// }
	// return oldNotification;
	// }
	//
	// // Delete an existing Notification
	// public Notification deleteNotification(int courseId) {
	// // create session
	// Session session = HibernateUtil.getHibernateSession();
	//
	// Notification oldNotification = null;
	// try {
	// // start a transaction
	// session.beginTransaction();
	//
	// oldNotification = (Notification) session.get(Notification.class, courseId);
	// // save the course object
	// session.delete(oldNotification);
	//
	// // commit transaction
	// session.getTransaction().commit();
	// } finally {
	// session.close();
	// }
	// return oldNotification;
	// }
}
