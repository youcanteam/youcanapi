package youcan.entities;
import java.io.Serializable;
import java.util.*;

import javax.persistence.*;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="course")
public class Course implements Serializable {
	@Id @GeneratedValue
	@Expose
	private int id;
	@Expose
	private String title;
	@Expose
	private String langage;
	private Date endDate;
	private String domaine;
	private String picturePath;
	private String description;
	private int seanceNumber;
	private boolean canSaveSession;
	private boolean isPrivate;
	private int price;
	private int minNbrContributors;
	private int maxNbrContributors;
	private String sylabPath;
	private String intructorName;
	private String intructorEmail;
	private String intructorPictureUrl;
	private int intructorId;
	private Date dateCreation;

	@Expose
	@OneToMany(mappedBy="course", fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Seance> seances = new ArrayList<>();
	
	@Expose
	@OneToMany(mappedBy="course", fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Comment> comments = new ArrayList<>();
	
	@Expose
	@OneToMany(mappedBy="course", fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Subscription> subscriptions = new ArrayList<>();
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "instructor_id")
	public User instructor;

	public Course() {
		super();
	}
	
	public Course(int courseId) {
		super();
		this.id = courseId;
	}
	
	public Course(String title, String langage, Date endDate, String domaine, String picturePath, String description,
			int seanceNumber, boolean canSaveSession, boolean isPrivate, int price, int minNbrContributors,
			int maxNbrContributors, String sylabPath, String intructorName, String intructorEmail,
			String intructorPictureUrl) {
		super();
		this.title = title;
		this.langage = langage;
		this.endDate = endDate;
		this.domaine = domaine;
		this.picturePath = picturePath;
		this.description = description;
		this.seanceNumber = seanceNumber;
		this.canSaveSession = canSaveSession;
		this.isPrivate = isPrivate;
		this.price = price;
		this.minNbrContributors = minNbrContributors;
		this.maxNbrContributors = maxNbrContributors;
		this.sylabPath = sylabPath;
		this.intructorName = intructorName;
		this.intructorEmail = intructorEmail;
		this.intructorPictureUrl = intructorPictureUrl;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getLangage() {
		return langage;
	}
	public void setLangage(String langage) {
		this.langage = langage;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getDomaine() {
		return domaine;
	}
	public void setDomaine(String domaine) {
		this.domaine = domaine;
	}
	public String getPicturePath() {
		return picturePath;
	}
	public void setPicturePath(String picturePath) {
		this.picturePath = picturePath;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getSeanceNumber() {
		return seanceNumber;
	}
	public void setSeanceNumber(int seanceNumber) {
		this.seanceNumber = seanceNumber;
	}
	public boolean isCanSaveSession() {
		return canSaveSession;
	}
	public void setCanSaveSession(boolean canSaveSession) {
		this.canSaveSession = canSaveSession;
	}
	public boolean isPrivate() {
		return isPrivate;
	}
	public void setPrivate(boolean isPrivate) {
		this.isPrivate = isPrivate;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getMinNbrContributors() {
		return minNbrContributors;
	}
	public void setMinNbrContributors(int minNbrContributors) {
		this.minNbrContributors = minNbrContributors;
	}
	public int getMaxNbrContributors() {
		return maxNbrContributors;
	}
	public void setMaxNbrContributors(int maxNbrContributors) {
		this.maxNbrContributors = maxNbrContributors;
	}
	public String getSylabPath() {
		return sylabPath;
	}
	public void setSylabPath(String sylabPath) {
		this.sylabPath = sylabPath;
	}
	public String getIntructorName() {
		return intructorName;
	}
	public void setIntructorName(String intructorName) {
		this.intructorName = intructorName;
	}
	public String getIntructorEmail() {
		return intructorEmail;
	}
	public void setIntructorEmail(String intructorEmail) {
		this.intructorEmail = intructorEmail;
	}
	public String getIntructorPictureUrl() {
		return intructorPictureUrl;
	}
	public void setIntructorPictureUrl(String intructorPictureUrl) {
		this.intructorPictureUrl = intructorPictureUrl;
	}
	public List<Seance> getSeances() {
		return seances;
	}
	public void setSeances(List<Seance> seances) {
		this.seances = seances;
	}

	public int getIntructorId() {
		//return this.getInstructor().getIdUser();
		return this.intructorId;
	}

	public void setIntructorId(int intructorId) {
		this.intructorId = intructorId;
	}

	public User getInstructor() {
		return instructor;
	}

	public void setInstructor(User instructor) {
		this.instructor = instructor;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComment(List<Comment> comments) {
		this.comments = comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public List<Subscription> getSubscriptions() {
		return subscriptions;
	}

	public void setSubscriptions(List<Subscription> subscriptions) {
		this.subscriptions = subscriptions;
	}
	
	

	

}
