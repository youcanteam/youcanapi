package youcan.entities;

import java.util.*;

import javax.persistence.*;

@Entity
@Table(name = "notification")
public class Notification {
	@Id
	@GeneratedValue
	private int id;
	private String message;
	private String entityName;
	private int entityId;
	private boolean canShowInBadge;
	private boolean isRequestAccepted;
	private Date notificationDatetime;
	private int userId;
	private int senderId;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "user_id")
	public User user;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "sender_id")
	public User sender;

	public Notification() {
		super();
	}

	public Notification(int id) {
		super();
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public int getEntityId() {
		return entityId;
	}

	public void setEntityId(int entityId) {
		this.entityId = entityId;
	}

	public boolean isCanShowInBadge() {
		return canShowInBadge;
	}

	public void setCanShowInBadge(boolean canShowInBadge) {
		this.canShowInBadge = canShowInBadge;
	}

	public boolean isRequestAccepted() {
		return isRequestAccepted;
	}

	public void setRequestAccepted(boolean isRequestAccepted) {
		this.isRequestAccepted = isRequestAccepted;
	}

	public Date getNotificationDatetime() {
		return notificationDatetime;
	}

	public void setNotificationDatetime(Date notificationDatetime) {
		this.notificationDatetime = notificationDatetime;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public User getSender() {
		return sender;
	}

	public void setSender(User sender) {
		this.sender = sender;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getSenderId() {
		return senderId;
	}

	public void setSenderId(int senderId) {
		this.senderId = senderId;
	}
	
	
}
