package youcan.entities;

import java.io.Serializable;
import java.util.*;

import javax.persistence.*;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.google.gson.annotations.Expose;

@Entity
@Table(name = "user")
public class User implements Serializable {

	@Id
	@GeneratedValue
	private int idUser;
	private String firstName;
	private String lastName;
	private String pictureUrl;
	private String email;
	private String speciality;
	private String sexe;
	private Date birthdate;
	private String country;
	private String city;
	private String about;
	private String password;

	@Expose
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "instructor", cascade = CascadeType.ALL)
	private List<Course> courses = new ArrayList<>();
	
	@Expose
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "instructor", cascade = CascadeType.ALL)
	private List<Comment> comments = new ArrayList<>();

	@ManyToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinTable(name = "user_contacts", joinColumns = { @JoinColumn(name = "idUser") }, inverseJoinColumns = {
			@JoinColumn(name = "idContact") })
	public List<User> contacts = new ArrayList<User>();
	
	@ManyToMany(mappedBy = "contacts")
	public List<User> users = new ArrayList<User>();

	@Expose
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	private List<Notification> notifications = new ArrayList<>();
	
	@Expose
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "sender", cascade = CascadeType.ALL)
	private List<Notification> sentNotifications = new ArrayList<>();
	// private List<User> contacts;

	//
	// @OneToMany(mappedBy="contact")
	// private transient List<User> contacts = new ArrayList<User>();
	//
	// @Column
	// @ElementCollection(targetClass=User.class)
	// public List<User> getContacts() {
	// return contacts;
	// }
	//
	// public void setContacts(List<User> contacts) {
	// this.contacts = contacts;
	// }

	public User() {
		super();
		// TODO Auto-generated constructor stub
	}

	public User(int id) {
		super();
		this.idUser = id;
	}

	public User(String firstName, String lastName, String email, String speciality, String sexe, Date birthdate,
			String country, String city, String about, String password) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.speciality = speciality;
		this.sexe = sexe;
		this.birthdate = birthdate;
		this.country = country;
		this.city = city;
		this.about = about;
		this.password = password;
	}

	public User(String firstName, String lastName, String email, String password) {

		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
	}

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFullName() {
		return  this.firstName + " " + this.lastName;
	}
	
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPictureUrl() {
		return pictureUrl;
	}

	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSpeciality() {
		return speciality;
	}

	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}

	public String getSexe() {
		return sexe;
	}

	public void setSexe(String sexe) {
		this.sexe = sexe;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public void addContact(User contact) {
		this.contacts.add(contact);
	}

	public List<Course> getCourses() {
		return courses;
	}

	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public List<User> getContacts() {
		return contacts;
	}

	public void setContacts(List<User> contacts) {
		this.contacts = contacts;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public List<Notification> getNotifications() {
		return notifications;
	}

	public void setNotifications(List<Notification> notifications) {
		this.notifications = notifications;
	}

	public List<Notification> getSentNotifications() {
		return sentNotifications;
	}

	public void setSentNotifications(List<Notification> sentNotifications) {
		this.sentNotifications = sentNotifications;
	}

}
