package youcan.entities;

import javax.persistence.*;

@Entity
@Table(name = "subscription")
public class Subscription {
	@Id
	@GeneratedValue
	private int id;
	private int userId;

	@ManyToOne
	@JoinColumn(name = "course_id")
	public Course course;

	public Subscription() {
		super();
	}

	public Subscription(int id) {
		super();
		this.id = id;
	}

	public Subscription(int userId, Course course) {
		super();
		this.userId = userId;
		this.course = course;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}
	

}
