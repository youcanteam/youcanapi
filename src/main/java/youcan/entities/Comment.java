package youcan.entities;

import java.util.*;

import javax.persistence.*;

import com.google.gson.annotations.Expose;

@Entity
@Table(name = "comment")
public class Comment {
	@Id
	@GeneratedValue
	private int id;
	private String message;
	private Date datePublished;
	private String instructorName;
	private String instructorPictureUrl;
	private int instructorId;

	@ManyToOne
	@JoinColumn(name = "course_id")
	public Course course;	
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "instructor_id")
	public User instructor;

	public Comment() {
		super();
	}

	public Comment(int id) {
		super();
		this.id = id;
	}

	public Comment(String message, Date datePublished, String instructorName, String instructorPictureUrl,
			int instructorId, Course course) {
		super();
		this.message = message;
		this.datePublished = datePublished;
		this.instructorName = instructorName;
		this.instructorPictureUrl = instructorPictureUrl;
		this.instructorId = instructorId;
		this.course = course;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getDatePublished() {
		return datePublished;
	}

	public void setDatePublished(Date datePublished) {
		this.datePublished = datePublished;
	}

	public String getInstructorName() {
		return instructorName;
	}

	public void setInstructorName(String instructorName) {
		this.instructorName = instructorName;
	}

	public String getInstructorPictureUrl() {
		return instructorPictureUrl;
	}

	public void setInstructorPictureUrl(String instructorPictureUrl) {
		this.instructorPictureUrl = instructorPictureUrl;
	}

	public int getInstructorId() {
		return instructorId;
	}

	public void setInstructorId(int instructorId) {
		this.instructorId = instructorId;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public User getInstructor() {
		return instructor;
	}

	public void setInstructor(User instructor) {
		this.instructor = instructor;
	}


}
