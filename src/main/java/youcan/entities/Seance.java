package youcan.entities;

import java.util.*;

import javax.persistence.*;

@Entity
@Table(name = "seance")
public class Seance {
	@Id
	@GeneratedValue
	private int id;
	private String title;
	private String duration;
	private Date startDate;
	private String startHour;
	private String description;

	@ManyToOne
	@JoinColumn(name = "course_id")
	public Course course;

	public Seance() {
		super();
	}

	public Seance(int id) {
		super();
		this.id = id;
	}

	public Seance(String title, String duration, Date startDate, String startHour, String description, Course course) {
		super();
		this.title = title;
		this.duration = duration;
		this.startDate = startDate;
		this.startHour = startHour;
		this.description = description;
	}

	public Seance(Course course) {
		super();
		this.setCourse(course);
		this.title = course.getTitle();
		this.duration = "0";
		this.startDate = new Date();
		this.startHour = "08:45";
		this.description = null;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}
	
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getStartHour() {
		return startHour;
	}

	public void setStartHour(String startHour) {
		this.startHour = startHour;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setCourse(Course course) {
		this.course = course;
	}
	
	public Course getCourse() {
		return course;
	}
}
