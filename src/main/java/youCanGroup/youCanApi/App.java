package youCanGroup.youCanApi;

import static spark.Spark.*;

import youcan.controllers.*;
import youcan.services.CommentService;
import youcan.services.CourseService;
import youcan.services.NotificationService;
import youcan.services.SeanceService;
import youcan.services.UserService;
import youcan.utils.*;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) {
        port(getHerokuAssignedPort());
        
	    CorsFilter.enableCORS("*", "*", "*"); 
        get("/", (req, res) -> "Welcome to Youcan API", new JsonTransformer());
	    	new CourseController(new CourseService());
	    	new UserController(new UserService(), new NotificationService());
	    	new SeanceController(new SeanceService(), new CourseService());
	    	new CommentController(new CommentService(), new CourseService(), new NotificationService());
	    	new NotificationController(new NotificationService());
	}
    
    static int getHerokuAssignedPort() {
        ProcessBuilder processBuilder = new ProcessBuilder();
        if (processBuilder.environment().get("PORT") != null) {
            return Integer.parseInt(processBuilder.environment().get("PORT"));
        }
        return 4567; //return default port if heroku-port isn't set (i.e. on localhost)
    }

}